#include "geometrica.hpp"

using namespace std;

Geometrica::Geometrica(){
	base = 0;
	altura = 0;
}

Geometrica::Geometrica(float base, float altura){
	this->base = base;
	this->altura = altura;
}
float Geometrica::getBase(){
	return base;
}
float Geometrica::getAltura(){
	return altura;
}
void Geometrica::setBase(float base){
	this->base = base;
}

void Geometrica::setAltura(float altura){
	this->altura = altura;
}


