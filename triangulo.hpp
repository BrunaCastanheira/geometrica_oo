#ifndef _TRIANGULO_H_
#define _TRIANGULO_H_

#include "geometrica.hpp"

class Triangulo : public Geometrica{
	public:
		Triangulo();
		Triangulo(float base, float altura);
		float Area();
		float Area(float base, float altura);
};

#endif
