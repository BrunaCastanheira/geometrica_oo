#ifndef _RETANGULO_H_
#define _RETANGULO_H_

#include "geometrica.hpp"

class Retangulo : public Geometrica{
	public:
		Retangulo();
		Retangulo(float base, float altura);
		float Area();
		float Area(float base, float altura);
};

#endif
