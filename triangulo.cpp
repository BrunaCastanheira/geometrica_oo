#include "triangulo.hpp"

using namespace std;

Triangulo::Triangulo(){
	setBase(10);
	setAltura(10);
}

Triangulo::Triangulo(float base, float altura){
	setBase(base);
	setAltura(altura);
}

float Triangulo::Area(){
	return getBase() * getAltura() / 2;
}

float Triangulo::Area(float base, float altura){
	return base * altura / 2;
}
