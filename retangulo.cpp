#include "retangulo.hpp"

using namespace std;

Retangulo::Retangulo(){
	setBase(10);
	setAltura(10);
}

Retangulo::Retangulo(float base, float altura){
	setBase(base);
	setAltura(altura);
}

float Retangulo::Area(){
	return getBase() * getAltura();
}

float Quadrado::area(float base, float altura){
	return base * altura;
}
