#ifndef _GEOMETRICA_H_
#define _GEOMETRICA_H_

#include <iostream>

using namespace std;

class Geometrica{
	private:
		float altura, base;
	public:
		Geometrica();
		Geometrica(float base, float altura);
		float getBase();
		void setBase(float base);
		float getAltura();
		void setAltura(float altura);

		virtual float Area() = 0;
};

#endif
